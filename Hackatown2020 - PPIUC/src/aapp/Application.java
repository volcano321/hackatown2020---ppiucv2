package aapp;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import neural_network.Cassandra;
import neural_network.Training;
import utilities.Accident;
import utilities.CalculPoints;
import utilities.Constantes;
import utilities.CsvReader;
import utilities.Stats;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JFormattedTextField;
import java.awt.Font;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import javax.swing.JButton;
import java.awt.event.ActionListener;

import java.awt.event.ActionEvent;
import javax.swing.JTabbedPane;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.border.MatteBorder;
import javax.swing.JProgressBar;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;


public class Application extends JFrame {

	private JPanel lblNbLeger;
	private JPanel stats;
	private JLabel lblMeteo;
	private JLabel lblHeure;
	private JLabel lblDate;
	private JLabel lblJour;
	private JLabel lblNbMort;
	private JLabel lblNbGrave;
	private JLabel lblLeger;
	private JLabel lblGravite;
	private JLabel lblNbVehicule;
	private JButton btnNiveauRisque;
	private JLabel lblRisque;
	private static CsvReader csv;
	private static Cassandra cas;
	private JButton btnEntrainer;
	private JComboBox comboHeure;
	private JComboBox comboMois;
	private JComboBox comboJour;
	private JComboBox comboSemaine;
	private String meteo="",jour="",date="",heure="";
	private JTabbedPane tabbedPane;
	private JLabel ch1;
	private JLabel label;
	private JLabel label_1;
	private JLabel label_2;
	private JLabel lblRseauNonEntrain;
	/*private JFormattedTextField rep1;
	private JFormattedTextField rep2;
	private JFormattedTextField rep3;
	private JFormattedTextField rep4;
	private JFormattedTextField rep5;
	private JFormattedTextField rep6;
	private JFormattedTextField rep7;*/

	/**
	 * Launch the application.
	 * @throws IOException 
	 */

	public static void main(String[] args) throws IOException {
		csv = new CsvReader();
		cas = new Cassandra();
		

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Application frame = new Application();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				//Essaie
				
				
				
			}		
				
			
		});
	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 */
	public Application() throws IOException {
		
		setBounds(new Rectangle(0, 0, 1280, 720));
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(new Rectangle(0, 0, 1280, 720));
		setContentPane(tabbedPane);
		lblNbLeger = new JPanel();
		lblNbLeger.setBounds(new Rectangle(0, 0, 1280, 720));
		lblNbLeger.setBackground(Color.WHITE);
		lblNbLeger.setBorder(null);
		lblNbLeger.setLayout(null);
		tabbedPane.addTab("Cassandra", null, lblNbLeger, "Does nothing");
		

		stats = new JPanel();
		stats.setBounds(new Rectangle(0, 0, 1280, 720));
		stats.setBackground(Color.WHITE);
		stats.setBorder(null);
		stats.setLayout(null);
		
		tabbedPane.addTab("Statistiques", null, stats, "Does nothing");

		ch1 = new JLabel(new ImageIcon(ImageIO.read((Application.class.getClassLoader().getResource("c1.png"))).getScaledInstance(500, 300, Image.SCALE_SMOOTH)));
		ch1.setBounds(20, 20, 500, 300);
		stats.add(ch1);
		
		label = new JLabel(new ImageIcon(ImageIO.read((Application.class.getClassLoader().getResource("c2.png"))).getScaledInstance(500, 300, Image.SCALE_SMOOTH)));
		label.setBounds(20, 320, 500, 300);
		stats.add(label);
		
		label_1 = new JLabel(new ImageIcon(ImageIO.read((Application.class.getClassLoader().getResource("c3.png"))).getScaledInstance(500, 300, Image.SCALE_SMOOTH)));
		label_1.setBounds(520, 20, 500, 300);
		stats.add(label_1);
		
		label_2 = new JLabel(new ImageIcon(ImageIO.read((Application.class.getClassLoader().getResource("c4.png"))).getScaledInstance(500, 300, Image.SCALE_SMOOTH)));
		label_2.setBounds(520, 320, 500, 300);
		stats.add(label_2);
		
		lblMeteo = new JLabel("Meteo");
		lblMeteo.setBounds(23, 184, 46, 14);
		lblNbLeger.add(lblMeteo);
		
		lblHeure = new JLabel("Heure");
		lblHeure.setBounds(23, 238, 46, 14);
		lblNbLeger.add(lblHeure);
		
		lblDate = new JLabel("Date");
		lblDate.setBounds(23, 290, 64, 14);
		lblNbLeger.add(lblDate);
		
		lblJour = new JLabel("Jour");
		lblJour.setBounds(23, 345, 46, 14);
		lblNbLeger.add(lblJour);
		
		lblNbMort = new JLabel("");
		lblNbMort.setBounds(1168, 184, 75, 14);
		lblNbLeger.add(lblNbMort);
		
		lblNbGrave = new JLabel("");
		lblNbGrave.setBounds(1168, 215, 46, 14);
		lblNbLeger.add(lblNbGrave);
		
		lblLeger = new JLabel("");
		lblLeger.setBounds(1168, 253, 46, 14);
		lblNbLeger.add(lblLeger);
		
		lblGravite = new JLabel("");
		lblGravite.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		lblGravite.setBounds(1166, 99, 88, 14);
		lblNbLeger.add(lblGravite);
		
		
		lblRisque = new JLabel("Niveau de risque : ");
		lblRisque.setBounds(23, 607, 220, 23);
		lblNbLeger.add(lblRisque);
		
		btnEntrainer = new JButton("Entrainer");
		btnEntrainer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				csv.lireAccidents(cas);
				lblRseauNonEntrain.setForeground(Color.green);
				lblRseauNonEntrain.setText("R�seau entrain�!");
			}
		});
		btnEntrainer.setBounds(1056, 453, 89, 23);
		lblNbLeger.add(btnEntrainer);
		
		JComboBox comboMeteo = new JComboBox();
		comboMeteo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				meteo = Integer.toString(comboMeteo.getSelectedIndex()+10);
			}
		});
		comboMeteo.setModel(new DefaultComboBoxModel(new String[] {"N/A", "Clair", "Couvert", "Brouillard", "Pluie", "Averse", "Vent Fort", "Neige", "Temp�te de Neige", "Verglas", "Autre"}));
		comboMeteo.setBounds(93, 179, 150, 27);
		lblNbLeger.add(comboMeteo);
		
		comboHeure = new JComboBox();
		comboHeure.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				heure = Integer.toString(comboHeure.getSelectedIndex());
			}
		});
		comboHeure.setModel(new DefaultComboBoxModel(new String[] {"N/A", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"}));
		comboHeure.setBounds(93, 233, 150, 27);
		lblNbLeger.add(comboHeure);
		
		comboMois = new JComboBox();
		comboMois.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				date = (String)comboMois.getSelectedItem()+"/"+comboJour.getSelectedItem();
			}
		});
		comboMois.setModel(new DefaultComboBoxModel(new String[] {"N/A", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"}));
		comboMois.setBounds(93, 285, 75, 27);
		lblNbLeger.add(comboMois);
		
		comboJour = new JComboBox();
		comboJour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				date = (String)comboMois.getSelectedItem()+"/"+comboJour.getSelectedItem();
			}
		});
		comboJour.setModel(new DefaultComboBoxModel(new String[] {"N/A", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"}));
		comboJour.setBounds(170, 285, 73, 27);
		lblNbLeger.add(comboJour);
		
		JLabel lblNewLabel_1 = new JLabel("Mois");
		lblNewLabel_1.setFont(new Font("Lucida Grande", Font.PLAIN, 8));
		lblNewLabel_1.setBounds(100, 272, 29, 16);
		lblNbLeger.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Jour");
		lblNewLabel_2.setFont(new Font("Lucida Grande", Font.PLAIN, 8));
		lblNewLabel_2.setBounds(176, 272, 61, 16);
		lblNbLeger.add(lblNewLabel_2);
		
		comboSemaine = new JComboBox();
		comboSemaine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch(comboSemaine.getSelectedIndex()) {
				case 0:
					jour = "DI";
					break;
				case 1:
					jour = "LU";
					break;
				case 2:
					jour = "MA";
					break;
				case 3:
					jour = "ME";
					break;
				case 4:
					jour = "JE";
					break;
				case 5:
					jour = "VE";
					break;
				case 6:
					jour = "SA";
					break;
				}
			
			}
		});
		comboSemaine.setModel(new DefaultComboBoxModel(new String[] {"N/A", "Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"}));
		comboSemaine.setBounds(93, 340, 150, 27);
		lblNbLeger.add(comboSemaine);
		
		PanelMap panelMap = new PanelMap();
		panelMap.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				Point2D.Double point = panelMap.setPointSouris(arg0.getX(), arg0.getY());
				
				
				int meteo2 = Integer.parseInt(meteo);
				if(meteo2 == 99) {
					meteo2 =10;
				} else {
					meteo2-=10;
				}
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
				String date2 = "2000/"+date;
				int daynb = LocalDate.parse(date2 , formatter).getDayOfYear();
				
				Accident acc = new Accident(comboSemaine.getSelectedIndex(), daynb , Integer.parseInt(heure),
				meteo2, 0, 0, 0, 0, 0, point.x,  point.y);
		
				acc.setNbPoints(CalculPoints.calculPoint(new Point2D.Double(acc.getLatitude(), acc.getLongitude()),csv.getLongLat()));
				cas.setAccidentEntree(acc);
				cas.calculNeuronne();
				lblNbMort.setText(Long.toString(Math.round(cas.getCoucheSortie()[0].getA())));
				lblNbGrave.setText(Long.toString(Math.round(cas.getCoucheSortie()[1].getA())));
				lblLeger.setText(Long.toString(Math.round(cas.getCoucheSortie()[2].getA())));
				lblGravite.setText(Long.toString(Math.round(cas.getCoucheSortie()[3].getA())));
				lblNbVehicule.setText(Long.toString(Math.round(cas.getCoucheSortie()[4].getA())));
				System.out.println(jour);
				double risque = Stats.risque(date, jour, heure,meteo,csv.GetAccidentsHash() , csv.getLongLat(),
						point.x,point.y);
				lblRisque.setText("Niveau de risque : " + String.format("%.4f", risque) + "%");
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				panelMap.updatePositionCercle(e.getX(), e.getY());
				panelMap.setDessinerCercle(true);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				panelMap.setDessinerCercle(false);
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		panelMap.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent arg0) {
				//debut
				//panelMap.updatePositionCercle((int)(arg0.getX() - panelMap.getLocation().getX()), (int)(arg0.getY() - panelMap.getLocation().getY()));
				panelMap.updatePositionCercle(arg0.getX(), arg0.getY());
				//fin
			}
		});
		panelMap.setBounds(255, 22, 704, 550);
		lblNbLeger.add(panelMap);
		
		lblNbVehicule = new JLabel("");
		lblNbVehicule.setBounds(1166, 289, 61, 16);
		lblNbLeger.add(lblNbVehicule);
		
		JLabel lblNewLabel_3 = new JLabel("Nombre de morts:");
		lblNewLabel_3.setBounds(971, 183, 156, 16);
		lblNbLeger.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Nombre de blessures graves:");
		lblNewLabel_4.setBounds(971, 214, 196, 16);
		lblNbLeger.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Nombre de blessures l�g�res:");
		lblNewLabel_5.setBounds(971, 252, 196, 16);
		lblNbLeger.add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Gravit� de la collision: ");
		lblNewLabel_6.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
		lblNewLabel_6.setBounds(971, 98, 214, 16);
		lblNbLeger.add(lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel("Nombre de v�hicule impliqu�:");
		lblNewLabel_7.setBounds(971, 288, 196, 16);
		lblNbLeger.add(lblNewLabel_7);
		
		lblRseauNonEntrain = new JLabel("R\u00E9seau non entrain\u00E9");
		lblRseauNonEntrain.setForeground(Color.RED);
		lblRseauNonEntrain.setBounds(1051, 426, 134, 14);
		lblNbLeger.add(lblRseauNonEntrain);
//		btnReseau.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent arg0) {
//				
//				int meteo2 = Integer.parseInt(meteo);
//				if(meteo2 == 99) {
//					meteo2 =10;
//				} else {
//					meteo2-=10;
//				}
//				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
//				date = "2000/"+date;
//				int daynb = LocalDate.parse(date , formatter).getDayOfYear();
//				
//				Accident acc = new Accident(comboSemaine.getSelectedIndex(), daynb , Integer.parseInt(heure),
//				meteo2, 0, 0, 0, 0, 0, Double.parseDouble(InLat.getText()),  Double.parseDouble(InLong.getText()));
//				acc.setNbPoints(CalculPoints.calculPoint(new Point2D.Double(acc.getLatitude(), acc.getLongitude()),csv.getLongLat()));
//				cas.setAccidentEntree(acc);
//				cas.calculNeuronne();
//				lblNbMort.setText(Long.toString(Math.round(cas.getCoucheSortie()[0].getA())));
//				lblNbGrave.setText(Long.toString(Math.round(cas.getCoucheSortie()[1].getA())));
//				lblLeger.setText(Long.toString(Math.round(cas.getCoucheSortie()[2].getA())));
//				lblGravite.setText(Long.toString(Math.round(cas.getCoucheSortie()[3].getA())));
//				lblNbVehicule.setText(Long.toString(Math.round(cas.getCoucheSortie()[4].getA())));
//			}
//		});
		
		
		
		/*rep1 = new JFormattedTextField();
		rep1.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		rep1.setBounds(940, 70, 150, 19);
		contentPane.add(rep1);
		
		rep2 = new JFormattedTextField();
		rep2.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		rep2.setBounds(940, 125, 150, 19);
		contentPane.add(rep2);
		
		rep3 = new JFormattedTextField();
		rep3.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		rep3.setBounds(940, 180, 150, 19);
		contentPane.add(rep3);
		
		rep4 = new JFormattedTextField();
		rep4.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		rep4.setBounds(940, 286, 150, 19);
		contentPane.add(rep4);
		
		rep5 = new JFormattedTextField();
		rep5.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		rep5.setBounds(940, 403, 150, 19);
		contentPane.add(rep5);
		
		rep6 = new JFormattedTextField();
		rep6.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		rep6.setBounds(940, 341, 150, 19);
		contentPane.add(rep6);
		
		rep7 = new JFormattedTextField();
		rep7.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		rep7.setBounds(940, 234, 150, 19);
		contentPane.add(rep7);*/
		
		
		
	


	}
}
