package aapp;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import utilities.CalculPoints;

public class PanelMap extends JPanel {
	private BufferedImage map;
	private Ellipse2D.Double cercleSouris;
	
	private  double rayonX, rayonY;
	private final Color couleurCercle = Color.red;
	private boolean doitDessinerCercle = false;
	private boolean premiereFois = true;
	
	private Color couleur = Color.black, couleurPrec;
	
	private final Point2D.Double POINT_REF_HD = new Point2D.Double(-73.484051, 45.706675);
	private final Point2D.Double POINT_REF_BG = new Point2D.Double(-73.955057, 45.404259);
	private double degreParPixelX, degreParPixelY; 

	/**
	 * Create the panel.
	 */
	public PanelMap() {
		try {
			//imagePath = new URL(System.getProperty("user.dir") + "\\Resources\\Montreal.JPG");
			//File file = new File(System.getProperty("user.dir") + "/Resources/Montreal.JPG");
			map = ImageIO.read(new File("./Resources/Montreal.JPG"));
			
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		if (premiereFois) {
			degreParPixelX =  (POINT_REF_BG.getX()-POINT_REF_HD.getX())/getWidth();
			degreParPixelY = (POINT_REF_BG.getY()-POINT_REF_HD.getY())/getHeight();
			
			rayonX = Math.abs(CalculPoints.rayon/degreParPixelX);
			rayonY = Math.abs(CalculPoints.rayon/degreParPixelY);
			premiereFois = false;
			System.out.println("RAYON Y: " + rayonY);
		}
		
		g2d.drawImage(map, 0, 0, getWidth(), getHeight(), 0, 0, map.getWidth(), map.getHeight(), null);
		if (doitDessinerCercle) {
			couleurPrec = g2d.getColor();
			g2d.setColor(new Color(couleurCercle.getRed(), couleurCercle.getGreen(), couleurCercle.getBlue(), 50));
			g2d.setStroke(new BasicStroke(2));
			g2d.fill(cercleSouris);
			g2d.setColor(couleurCercle);
			g2d.draw(cercleSouris);
			g2d.setStroke(new BasicStroke(1));
			g2d.setColor(couleurPrec);
		}
		
		
		
	}
	
	public void updatePositionCercle(int x, int y) {
		cercleSouris = new Ellipse2D.Double(x - rayonX/2, y - rayonY/2, rayonX, rayonY);
		repaint();
	}
	
	public void setDessinerCercle(boolean value) {
		doitDessinerCercle = value;
		repaint();
	}
	
	public Point2D.Double setPointSouris(int x, int y) {
		return calculApproxPos(new Point2D.Double(x, y));
	}
	
	private Point2D.Double calculApproxPos(Point2D.Double coor) {
		double posRelX = POINT_REF_BG.getX() - coor.getX()*degreParPixelX;
		double posRelY = POINT_REF_BG.getY() - coor.getY()*degreParPixelY;
		System.out.println("POS REL X: " + posRelX);
		System.out.println("POS REL Y: " + posRelY);
		return new Point2D.Double(posRelX,posRelY);
		
	}

}
