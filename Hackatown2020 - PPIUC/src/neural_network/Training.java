package neural_network;

import utilities.Accident;

public class Training {
	
	public static final int AMOUNT_IN_BATCH = 100;
	public static final int NO_OF_BATCHES = 10000;
	public static int compteurBatch = 0;
	
	public static void trainStep(Cassandra cas,Accident acc) 
	{
		cas.setAccidentEntree(acc);
		cas.calculNeuronne();
		
		
		//System.out.println(cas.calculCout(acc));
		cas.calculTriangle(acc);

		compteurBatch ++;
		if (compteurBatch == AMOUNT_IN_BATCH) {
			cas.weightbiase();
			compteurBatch = 0;
		}
	}
}
