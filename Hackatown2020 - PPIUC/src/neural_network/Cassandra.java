package neural_network;

import java.util.Random;

import utilities.Accident;
import utilities.Constantes;

public class Cassandra {

	public final int NBR_COUCHES = 3;
	public final int NBR_NEURONE_ENTREE = 5; // � d�terminer
	public final int NBR_NEURONE_INTER = 5;
	public final int NBR_NEURONE_SORTIE = 5; // � d�terminer
	public final int NBR_NEURONE_TOTAL = NBR_NEURONE_INTER + NBR_NEURONE_SORTIE + NBR_NEURONE_ENTREE;
	public final double alpha = 0.1;

	public class Neurone {
		private double a, b, triangle;
		private double[] w;

		public Neurone() {
			a = 0;
			b = 0;
			triangle = 0;
		}

		public String toString() {
			String str = "";
			if (w != null) {
				for (int i = 0; i < w.length; i++) {
					str += w[i] + " ";
				}
			}
			return "a: " + a + " b: " + b + " triangle: " + triangle + " w: " + str;
		}

		/**
		 * @return the a
		 */
		public double getA() {
			return a;
		}
		
		
	}

	private Neurone[] coucheEntree = new Neurone[NBR_NEURONE_ENTREE];
	private Neurone[] coucheInter = new Neurone[NBR_NEURONE_INTER];
	private Neurone[] coucheSortie = new Neurone[NBR_NEURONE_SORTIE];
	private Random rand = new Random();

	public Cassandra() {
		for (int i = 0; i < coucheEntree.length; i++) {
			coucheEntree[i] = new Neurone();
		}
		for (int i = 0; i < coucheInter.length; i++) {
			coucheInter[i] = new Neurone();
			coucheInter[i].w = new double[NBR_NEURONE_ENTREE];
			for (int j = 0; j < coucheInter[i].w.length; j++) {
				coucheInter[i].w[j] = (rand.nextDouble()) * Math.sqrt(2.0 / NBR_NEURONE_ENTREE);
			}
			coucheInter[i].b = 0;
		}

		for (int i = 0; i < coucheSortie.length; i++) {
			coucheSortie[i] = new Neurone();
			coucheSortie[i].w = new double[NBR_NEURONE_INTER];
			for (int j = 0; j < coucheSortie[i].w.length; j++) {
				coucheSortie[i].w[j] = (rand.nextDouble()) * Math.sqrt(2.0 / NBR_NEURONE_INTER);
			}
			coucheSortie[i].b = 0;
		}
	}

	public void setAccidentEntree(Accident accident) {
		coucheEntree[0].a = accident.getJourDeSemaine() / Constantes.NBR_JOURSEMAINE;
		coucheEntree[1].a = accident.getDate() / Constantes.NBR_JOUR;
		coucheEntree[2].a = accident.getHeure() / Constantes.NBR_HEURES;
		coucheEntree[3].a = accident.getMeteo() / Constantes.NBR_METEO;
		coucheEntree[4].a = accident.getNbPoints() /Constantes.NBR_MAX_POINTS;

	}

	public void calculNeuronne() {
		for (Neurone neurone : coucheInter) {
			double zNouv = 0;
			for (int i = 0; i < coucheEntree.length; i++) {
				zNouv += neurone.w[i] * coucheEntree[i].a + neurone.b;
			}
			neurone.a = sigmoid(zNouv);
		}

		for (Neurone neurone : coucheSortie) {
			double zNouv = 0;
			for (int i = 0; i < coucheInter.length; i++) {
				zNouv += neurone.w[i] * coucheInter[i].a + neurone.b;
			}
			neurone.a = zNouv;
		}
	}

	public double calculCout(Accident accident) {
		//System.out.println(toString(2));
		double coutTemp = 0;
		coutTemp += (coucheSortie[0].a - accident.getNbMorts()) * (coucheSortie[0].a - accident.getNbMorts());
		coutTemp += (coucheSortie[1].a - accident.getnbBlessesLeges())
				* (coucheSortie[1].a - accident.getnbBlessesLeges());
		coutTemp += (coucheSortie[2].a - accident.getnbBlessesLeges())
				* (coucheSortie[2].a - accident.getNbBlessesGraves());
		coutTemp += (coucheSortie[3].a - accident.getGravite()) * (coucheSortie[3].a - accident.getGravite());
		coutTemp += (coucheSortie[4].a - accident.getNbAuto()) * (coucheSortie[4].a - accident.getNbAuto());
		return coutTemp / 2.0;

	}

	public void calculTriangle(Accident accident) {

		ajustementDerniereCouche(accident);

		for (int i = 0; i < coucheInter.length; i++) {
			for (Neurone neur : coucheSortie) {
				coucheInter[i].triangle += neur.triangle * neur.w[i] * coucheInter[i].a * (1 - coucheInter[i].a);
			}
		}

	}

	public void weightbiase() {
		for (Neurone neur : coucheSortie) {
			for (int i = 0; i < coucheInter.length; i++) {
				neur.w[i] -= alpha * neur.triangle * coucheInter[i].a;
				neur.b -= alpha * neur.triangle;
				neur.triangle = 0;
			}
		}

		for (Neurone neur : coucheInter) {
			for (int i = 0; i < coucheEntree.length; i++) {
				neur.w[i] -= alpha * neur.triangle * coucheEntree[i].a;
				neur.b -= alpha * neur.triangle;
				neur.triangle = 0;
			}
		}

	}

	private void ajustementDerniereCouche(Accident accident) {

		coucheSortie[0].triangle = (coucheSortie[0].a - accident.getNbMorts()) * sigmoid(coucheSortie[0].a)
				* (1 - sigmoid(coucheSortie[0].a));
		coucheSortie[1].triangle = (coucheSortie[1].a - accident.getnbBlessesLeges()) * sigmoid(coucheSortie[1].a)
				* (1 - sigmoid(coucheSortie[1].a));
		coucheSortie[2].triangle = (coucheSortie[2].a - accident.getnbBlessesLeges()) * sigmoid(coucheSortie[2].a)
				* (1 - sigmoid(coucheSortie[2].a));
		coucheSortie[3].triangle = (coucheSortie[3].a - accident.getGravite()) * sigmoid(coucheSortie[3].a)
				* (1 - sigmoid(coucheSortie[3].a));
		coucheSortie[4].triangle = (coucheSortie[4].a - accident.getNbAuto()) * sigmoid(coucheSortie[4].a)
				* (1 - sigmoid(coucheSortie[4].a));
	}

	public String toString(int couche) {
		String str = "";
		switch (couche) {
		case 0:
			for (int i = 0; i < coucheEntree.length; i++) {
				str += i + " " + coucheEntree[i].toString() + "\n";
			}
			break;
		case 1:
			for (int i = 0; i < coucheInter.length; i++) {
				str += i + " " + coucheInter[i].toString() + "\n";
			}
			break;
		case 2:
			for (int i = 0; i < coucheSortie.length; i++) {
				str += i + " " + coucheSortie[i].toString() + "\n";
			}
			break;
		default:
			return "Invalide";
		}

		return str + "\n";
	}

	/**
	 * Fonction Math�matique sigmoid
	 * 
	 * @param value
	 *            Le nombre � �valu�
	 * @return Le sigmoid de value
	 */
	public double sigmoid(double value) {
		return 1 / (1 + Math.exp(-value));
	}

	/**
	 * @return the coucheSortie
	 */
	public Neurone[] getCoucheSortie() {
		return coucheSortie;
	}

	
	
}
