package utilities;

import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import java.util.HashMap;

import javax.swing.JLabel;
import javax.swing.JProgressBar;

import neural_network.Cassandra;
import neural_network.Training;


public class CsvReader {

	private HashMap<String, Integer>[] accidents = new HashMap[7];
	private boolean header = true;
	private ArrayList<Point2D.Double> longLat = new ArrayList<Point2D.Double>();
	
	public CsvReader() throws IOException 
	{
		String pathToCsv = ".\\Ressources\\accidents_2012_2018.csv";
		//String pathToCsv = "/Users/maxbrodeur/git/hackatown2020---ppiucv2/Hackatown2020 - PPIUC/Ressources/accidents_2012_2018.csv"; //Pour Macbook
		String row;
		BufferedReader csvReader = new BufferedReader(new FileReader(pathToCsv));
		for(int i = 0; i < accidents.length; i++)
		{
			accidents[i] = new HashMap<String, Integer>();
		}
		while ((row = csvReader.readLine()) != null) 
		{
			if(header)
			{
				header = false;
				continue;
			}
			String[] data = row.split(",");
			if(data[35].equals("Montréal(06)"))
			{
				//LONG
				if(data.length == 68)
				{
					String longitude = data[66];
				
					if (accidents[TypeAccident.Longitude.ordinal()].containsKey(longitude)) {
						accidents[TypeAccident.Longitude.ordinal()].put(longitude, accidents[TypeAccident.Longitude.ordinal()].get(longitude) + 1);
					}
					else
					{
						accidents[TypeAccident.Longitude.ordinal()].put(longitude, 1);
					}
					//LAT
					String latitude = data[67];
					Point2D.Double ptTemp = new Point2D.Double(Double.parseDouble(latitude), Double.parseDouble(longitude));
					longLat.add(ptTemp);
					if (accidents[TypeAccident.Latitude.ordinal()].containsKey(latitude)) {
						accidents[TypeAccident.Latitude.ordinal()].put(latitude, accidents[TypeAccident.Latitude.ordinal()].get(latitude) + 1);
					}
					else
					{
						accidents[TypeAccident.Latitude.ordinal()].put(latitude, 1);
					}
				}
				//JOUR SEMAINE
				String jourSemaine = data[1];
				if (accidents[TypeAccident.JourSemaine.ordinal()].containsKey(jourSemaine)) {
					accidents[TypeAccident.JourSemaine.ordinal()].put(jourSemaine, accidents[TypeAccident.JourSemaine.ordinal()].get(jourSemaine) + 1);
				}
				else
				{
					accidents[TypeAccident.JourSemaine.ordinal()].put(jourSemaine, 1);
				}
				//DATE
				String dateModif = data[2].substring(5);
				if (accidents[TypeAccident.Date.ordinal()].containsKey(dateModif)) {
					accidents[TypeAccident.Date.ordinal()].put(dateModif, accidents[TypeAccident.Date.ordinal()].get(dateModif) + 1);
				}
				else
				{
					accidents[TypeAccident.Date.ordinal()].put(dateModif, 1);
				}
				
				//Heure
				String heureStr = data[31].substring(0, 2);
				int heure = 0;
				try {
					heure = Integer.parseInt(heureStr) + 1;
				} catch (NumberFormatException e) {
					if (e.getClass().equals(NumberFormatException.class)) {
						heure = 0;
					}
				}
				if (accidents[TypeAccident.Heure.ordinal()].containsKey(heure + "")) {
					accidents[TypeAccident.Heure.ordinal()].put(heure + "", accidents[TypeAccident.Heure.ordinal()].get(heure + "") + 1);
				}
				else
				{
					accidents[TypeAccident.Heure.ordinal()].put(heure + "", 1);
				}
				
				
				//Meteo
				String meteo = data[26];
				if (meteo.isEmpty()) {
					meteo = "0";
				}
				if (accidents[TypeAccident.Meteo.ordinal()].containsKey(meteo)) {
					accidents[TypeAccident.Meteo.ordinal()].put(meteo, accidents[TypeAccident.Meteo.ordinal()].get(meteo) + 1);
				}
				else
				{
					accidents[TypeAccident.Meteo.ordinal()].put(meteo, 1);
				}
				
				
				

			}
		}
		csvReader.close();
		
	}
	


	public ArrayList<Point2D.Double> getLongLat()
	{
		
		return longLat;
	}
	

	public HashMap<String, Integer>[] GetAccidentsHash()
	{
		return accidents;
	}
	

	public void lireAccidents(Cassandra reseau) {
		String pathToCsv = ".\\Ressources\\accidents_2012_2018.csv";
		//String pathToCsv = "/Users/maxbrodeur/git/hackatown2020---ppiucv2/Hackatown2020 - PPIUC/Ressources/accidents_2012_2018.csv"; //Pour Macbook
		String row;
		BufferedReader csvReader;
		Accident accident;
		try {
			csvReader = new BufferedReader(new FileReader(pathToCsv));
			csvReader.readLine();
			while ((row = csvReader.readLine()) != null) {
				accident = new Accident();
				String[] rowData = row.split(",");


				if(rowData[35].equals("Montréal(06)"))
				{
					//JOUR SEMAINE
					String jourSemaine = rowData[1];
					switch (jourSemaine) {
					case "LU" : accident.setJourDeSemaine(1);
					break;
					case "MA" : accident.setJourDeSemaine(2);
					break;
					case "ME" : accident.setJourDeSemaine(3);
					break;
					case "JE" : accident.setJourDeSemaine(4);
					break;
					case "VE" : accident.setJourDeSemaine(5);
					break;
					case "SA" : accident.setJourDeSemaine(6);
					break;
					case "DI" : accident.setJourDeSemaine(7);
					break;
					}

					//DATE
					String date = rowData[2];
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
					accident.setDate(LocalDate.parse(date , formatter).getDayOfYear());

					//HEURE
					String heureStr = rowData[31].substring(0, 2);
					int heure = 0;
					try {
						heure = Integer.parseInt(heureStr) + 1;
					} catch (NumberFormatException e) {
						if (e.getClass().equals(NumberFormatException.class)) {
							heure = 0;
						}
					}
					accident.setHeure(heure);

					//METEO
					String meteoStr = rowData[26];
					if (meteoStr.isEmpty()) {
						accident.setMeteo(0);
					} else {
						int meteo = Integer.parseInt(meteoStr);
						if (meteo == 99) {
							accident.setMeteo(Meteo.AUTRE.ordinal());
						} else {
							accident.setMeteo(meteo - 10);
						}
					}


					//NB MORTS
					accident.setNbMorts(Integer.parseInt(rowData[28]));

					//NB BLESSES LEGES
					accident.setnbBlessesLeges(Integer.parseInt(rowData[30]));

					//NB BLESSES GRAVES
					accident.setNbBlessesGraves(Integer.parseInt(rowData[29]));

					//NB AUTOMOBILE
					String nbAutoStr = rowData[27];
					if (nbAutoStr.isEmpty()) {
						accident.setNbAuto(0);;
					} else {
						accident.setNbAuto(Integer.parseInt(nbAutoStr));
					}

					//GRAVITE
					String graviteStr = rowData[34];
					switch (graviteStr) {
					case "" : accident.setGravite(Gravite.NULL.ordinal());
					break;
					case "Dommages matériels inférieurs au seuil de rapportage" : accident.setGravite(Gravite.DOMMAGE_MINIME.ordinal());
					break;
					case "Dommages matériels seulement" : accident.setGravite(Gravite.DOMMAGE_MATERIEL.ordinal());
					break;
					case "Léger" : accident.setGravite(Gravite.LEGER.ordinal());
					break;
					case "Grave" : accident.setGravite(Gravite.GRAVE.ordinal());
					break;
					case "Mortel" : accident.setGravite(Gravite.MORTEL.ordinal());
					break;
					}
					//LONG et LAT
					if(rowData.length == 68)
					{
						String longitude = rowData[66];
						String latitude = rowData[67];

						accident.setLongitude(Double.parseDouble(longitude));
						accident.setLatitude(Double.parseDouble(latitude));
						accident.setNbPoints(CalculPoints.calculPoint(new Point2D.Double(accident.getLatitude(),accident.getLongitude()), getLongLat()));
					}



					Training.trainStep(reseau, accident);
					
				}
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	
	public enum TypeAccident
	{
		Longitude,
		Latitude,
		JourSemaine,
		Date,
		Heure,
		Meteo
	}


	
	public enum Meteo 
	{
		NULL,
		CLAIR,
		COUVERT,
		BROUILLARD,
		PLUIE,
		AVERSE,
		VENT,
		NEIGE,
		POUDRERIE,
		VERGLAS,
		AUTRE
	}
	
	public enum Gravite
	{
		NULL,
		DOMMAGE_MINIME,
		DOMMAGE_MATERIEL,
		LEGER,
		GRAVE,
		MORTEL
	}

}




