package utilities;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;

public class Stats {
	public static double risque(String date, String jour, String heure, String meteo, HashMap[] accidents, ArrayList<Point2D.Double> liste,
			double longitude, double latitude) {
		double sum = 0;
		sum += CalculPoints.calculPoint(new Point2D.Double(latitude, longitude), liste)/Constantes.NBR_MAX_POINTS;
		sum += (Integer) accidents[2].get(jour) / Constantes.NBR_TOTAL_ACCIDENT * Constantes.NBR_JOURSEMAINE;
		System.out.println(date);
		sum += (Integer) accidents[3].get(date) / Constantes.NBR_TOTAL_ACCIDENT * Constantes.NBR_JOUR;
		sum += (Integer) accidents[4].get(heure) / Constantes.NBR_TOTAL_ACCIDENT * Constantes.NBR_HEURES;
		sum += (Integer) accidents[5].get(meteo) / Constantes.NBR_TOTAL_ACCIDENT * Constantes.NBR_METEO;

		return 100 * sum / Constantes.NBR_CATEGORIE_TOTAL;

	}
}
