package utilities;

import java.awt.geom.Point2D;

public class Accident {
	private int jourDeSemaine;
	private int date;
	private int heure;
	private int meteo;
	private double longitude, latitude;
	private int nbPoints;
	
	private int nbMorts;
	private int nbBlessesLeges;
	private int nbBlessesGraves;
	private int gravite;
	private int nbAuto;
	
	



	public Accident(int jourDeSemaine, int date, int heure, int meteo, int nbMorts, int nbBlessesLeges, int nbBlessesGraves, int gravite, int nbAuto, double latitude, double longitude) {
		this.jourDeSemaine = jourDeSemaine;
		this.date = date;
		this.heure = heure;
		this.meteo = meteo;
		this.nbMorts = nbMorts;
		this.nbBlessesLeges = nbBlessesLeges;
		this.nbBlessesGraves = nbBlessesGraves;
		this.gravite = gravite;
		this.nbAuto = nbAuto;
		
	}


	public Accident() {
		// TODO Auto-generated constructor stub
	}
	
	public String toString() {
		return "Jour de semaine: " + jourDeSemaine + "\nDate: " + date + "\nHeure: " + heure + "\nLongitude: " + longitude + "\nMeteo: " + meteo + "\nVitesse: " +
				"\nNombre Morts: " + nbMorts + "\nNombre Bless�s l�g�s: " + nbBlessesLeges + "\nNombre Bless�s Graves: " + nbBlessesGraves + "\nGravit�: " + gravite + "\nNombre autos: " + nbAuto +
				"\nLatitude: " + latitude;
	}
	


	/**
	 * @return the jourDeSemaine
	 */
	public int getJourDeSemaine() {
		return jourDeSemaine;
	}


	/**
	 * @param jourDeSemaine the jourDeSemaine to set
	 */
	public void setJourDeSemaine(int jourDeSemaine) {
		this.jourDeSemaine = jourDeSemaine;
	}


	/**
	 * @return the date
	 */
	public int getDate() {
		return date;
	}


	/**
	 * @param date the date to set
	 */
	public void setDate(int date) {
		this.date = date;
	}


	/**
	 * @return the heure
	 */
	public int getHeure() {
		return heure;
	}


	/**
	 * @param heure the heure to set
	 */
	public void setHeure(int heure) {
		this.heure = heure;
	}


	/**
	 * @return the quartier
	 */
	public double getLongitude() {
		return longitude;
	}


	/**
	 * @param quartier the quartier to set
	 */
	public void setLatitude(double d) {
		this.latitude = d;
	}


	/**
	 * @return the rue
	 */
	public double getLatitude() {
		return latitude;
	}


	/**
	 * @param rue the rue to set
	 */
	public void setLongitude(double d) {
		this.longitude = d;
	}


	/**
	 * @return the meteo
	 */
	public int getMeteo() {
		return meteo;
	}


	/**
	 * @param meteo the meteo to set
	 */
	public void setMeteo(int meteo) {
		this.meteo = meteo;
	}


	/**
	 * @return the nbMorts
	 */
	public int getNbMorts() {
		return nbMorts;
	}


	/**
	 * @param nbMorts the nbMorts to set
	 */
	public void setNbMorts(int nbMorts) {
		this.nbMorts = nbMorts;
	}


	/**
	 * @return the nbBlesses
	 */
	public int getnbBlessesLeges() {
		return nbBlessesLeges;
	}


	/**
	 * @param nbBlesses the nbBlesses to set
	 */
	public void setnbBlessesLeges(int nbBlessesLeges) {
		this.nbBlessesLeges = nbBlessesLeges;
	}


	/**
	 * @return the gravite
	 */
	public int getGravite() {
		return gravite;
	}


	/**
	 * @param gravite the gravite to set
	 */
	public void setGravite(int gravite) {
		this.gravite = gravite;
	}


	/**
	 * @return the nbAuto
	 */
	public int getNbAuto() {
		return nbAuto;
	}


	/**
	 * @param nbAuto the nbAuto to set
	 */
	public void setNbAuto(int nbAuto) {
		this.nbAuto = nbAuto;
	}


	/**
	 * @return the nbBlessesGraves
	 */
	public int getNbBlessesGraves() {
		return nbBlessesGraves;
	}


	/**
	 * @param nbBlessesGraves the nbBlessesGraves to set
	 */
	public void setNbBlessesGraves(int nbBlessesGraves) {
		this.nbBlessesGraves = nbBlessesGraves;
	}


	/**
	 * @return the nbPoints
	 */
	public int getNbPoints() {
		return nbPoints;
	}


	/**
	 * @param nbPoints the nbPoints to set
	 */
	public void setNbPoints(int nbPoints) {
		this.nbPoints = nbPoints;
	}


	/**
	 * @return the nbBlessesLeges
	 */
	public int getNbBlessesLeges() {
		return nbBlessesLeges;
	}


	/**
	 * @param nbBlessesLeges the nbBlessesLeges to set
	 */
	public void setNbBlessesLeges(int nbBlessesLeges) {
		this.nbBlessesLeges = nbBlessesLeges;
	}
	
	
}
