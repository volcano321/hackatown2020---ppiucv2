package utilities;

public class Constantes {
	public static final double NBR_JOURSEMAINE = 7;
	public static final double NBR_HEURES = 24;
	public static final double NBR_JOUR = 365;
	public static final double NBR_LONG_LAT = 171172;
	public static final double NBR_METEO = 10;
	public static final double RATIO_AIRE = (CalculPoints.rayon/(0.5623814543612503/2))*(CalculPoints.rayon/(0.5623814543612503/2));

	public static final double NBR_CATEGORIE_TOTAL = NBR_JOURSEMAINE + NBR_HEURES + NBR_JOUR + NBR_METEO+RATIO_AIRE;
	public static final double NBR_TOTAL_ACCIDENT = 171172;
	public static final double NBR_MAX_POINTS = RATIO_AIRE*NBR_TOTAL_ACCIDENT;
}
