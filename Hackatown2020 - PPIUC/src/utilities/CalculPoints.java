package utilities;

import java.awt.geom.Point2D;
import java.util.ArrayList;

public class CalculPoints {
	public static final double rayon = .045;
	public static double distMax = 0;
	public static int calculPoint(Point2D.Double point,ArrayList<Point2D.Double> liste) 
	{
		int nbPoint = 0;
		for(Point2D.Double pt : liste){
				double dist = Math.sqrt((pt.x-point.x)*(pt.x-point.x) + (pt.y-point.y)*(pt.y-point.y));
				if(dist <= rayon) 
				{
					nbPoint++;
				}
				//if(dist>= distMax) {
				//	distMax = dist;
				//	System.out.println(distMax);
				//}
		}
		return nbPoint;
	}

}
